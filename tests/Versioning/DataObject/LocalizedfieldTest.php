<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning\DataObject;

use Exception;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\TestObject;
use Tests\Fittinq\Pimcore\Versioning\Versioning\Configuration;

class LocalizedfieldTest extends TestCase
{
    private TestObject $testObject;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = $this->configuration->configure();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/DataObject/Localizedfield');
    }

    /**
     * @throws Exception
     */
    public function test_returnTestLocalizedInputIfFieldChanges()
    {
        $this->testObject->setLocalizedfieldInput('old', 'nl_NL');
        $this->testObject->save();
        $this->testObject->setLocalizedfieldInput('new', 'nl_NL');
        $this->testObject->save();

        $this->assertEquals(['TestObject.localizedfieldInput.nl_NL', 'TestObject.localizedfieldInput'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestLocalizedTextareaIfFieldChanges()
    {
        $this->testObject->setLocalizedfieldTextarea('old', 'nl_NL');
        $this->testObject->save();
        $this->testObject->setLocalizedfieldTextarea('new', 'nl_NL');
        $this->testObject->save();

        $this->assertEquals(['TestObject.localizedfieldTextArea.nl_NL', 'TestObject.localizedfieldTextArea'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }
}
