<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Exceptions;

use RuntimeException;

class NotEnoughVersionsException extends RuntimeException
{
}
