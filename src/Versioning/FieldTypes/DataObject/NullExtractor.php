<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject;

use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;

class NullExtractor implements FieldTypeExtractor
{
    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array
    {
        return [];
    }
}
