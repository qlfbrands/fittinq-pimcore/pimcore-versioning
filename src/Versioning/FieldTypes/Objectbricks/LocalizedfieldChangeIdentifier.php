<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Objectbricks;

class LocalizedfieldChangeIdentifier
{
    public function getChangedFields(array $lhs, array $rhs): array
    {
        $changesByLocale = [];

        foreach ($lhs as $key => $lhsByLocale){
            $rhsByLocale = array_shift($rhs);
            $differences = array_keys(array_diff($lhsByLocale, $rhsByLocale));

            if (!empty($differences)){
                $changesByLocale[] = [$key => $differences];
            }
        }

        return $changesByLocale;
    }
}
