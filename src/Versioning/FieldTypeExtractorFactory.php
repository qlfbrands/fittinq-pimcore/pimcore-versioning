<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning;

use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Block\Block;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Block\BlockChangeIdentifier;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\Checkbox;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\Input;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\ManyToMany;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\ManyToOne;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\Multiselect;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\NullExtractor;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\Number;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\QuantityValue;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject\Select;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Localizedfields\Localizedfields;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Objectbricks\Objectbricks;

class FieldTypeExtractorFactory
{
    public function getFieldTypeExtractor(string $fieldType): FieldTypeExtractor
    {
        return match ($fieldType) {
            FieldType::MANY_TO_ONE_RELATION => new ManyToOne(),
            FieldType::MANY_TO_MANY_RELATION, FieldType::MANY_TO_MANY_OBJECT_RELATION => new ManyToMany(),
            FieldType::CHECKBOX, FieldType::NEWSLETTER_CONFIRMED, FieldType::NEWSLETTER_ACTIVE => new Checkbox(),
            FieldType::INPUT, FieldType::TEXTAREA => new Input(),
            FieldType::NUMBER => new Number(),
            FieldType::SELECT => new Select(),
            FieldType::QUANTITYVALUE => new QuantityValue(),
            FieldType::MULTISELECTION => new Multiselect(),
            FieldType::OBJECTBRICKS => new Objectbricks(),
            FieldType::LOCALIZEDFIELDS => new Localizedfields(),
            FieldType::BLOCK => new Block(),
            default => new NullExtractor(),
        };
    }
}
